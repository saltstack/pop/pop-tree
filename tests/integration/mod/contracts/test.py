def pre(hub, ctx):
    ...


def pre_module_func(hub, ctx):
    ...


def call(hub, ctx):
    return ctx.func(*ctx.args, **ctx.kwargs)


def post(hub, ctx):
    return ctx.ret


def post_module_func(hub, ctx):
    return ctx.ret
