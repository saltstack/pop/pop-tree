def test_str_alias(hub, cli_doc):
    assert hub.tree.test.str_alias(param=1) == 1
    assert hub.tree.test.str_alias(p=1) == 1
    assert hub.tree.test.str_alias(1) == 1

    assert hub.tree.test.str_alias.param_aliases["param"] == {"param", "p"}
    ret = cli_doc("tree.test.str_alias", "--output=json")
    assert ret["parameters"]["param"]["aliases"] == ["p", "param"]
    assert "annotation" not in ret["parameters"]["param"]


def test_multiple_str_alias(hub, cli_doc):
    assert hub.tree.test.multiple_str_alias(param=1) == 1
    assert hub.tree.test.multiple_str_alias(p=1) == 1
    assert hub.tree.test.multiple_str_alias(prm=1) == 1
    assert hub.tree.test.multiple_str_alias(1) == 1

    assert hub.tree.test.multiple_str_alias.param_aliases["param"] == {
        "param",
        "p",
        "prm",
    }
    ret = cli_doc("tree.test.multiple_str_alias", "--output=json")
    assert ret["parameters"]["param"]["aliases"] == ["p", "param", "prm"]
    assert "annotation" not in ret["parameters"]["param"]


def test_tuple_alias(hub, cli_doc):
    assert hub.tree.test.tuple_alias(param=1) == 1
    assert hub.tree.test.tuple_alias(p=1) == 1
    assert hub.tree.test.tuple_alias(1) == 1

    assert hub.tree.test.tuple_alias.param_aliases["param"] == {"param", "p"}

    ret = cli_doc("tree.test.tuple_alias", "--output=json")
    assert ret["parameters"]["param"]["aliases"] == ["p", "param"]
    assert ret["parameters"]["param"]["annotation"] == "<class 'str'>"


def test_multiple_tuple_alias(hub, cli_doc):
    assert hub.tree.test.multiple_tuple_alias(param=1) == 1
    assert hub.tree.test.multiple_tuple_alias(p=1) == 1
    assert hub.tree.test.multiple_tuple_alias(prm=1) == 1
    assert hub.tree.test.multiple_tuple_alias(1) == 1

    assert hub.tree.test.multiple_tuple_alias.param_aliases["param"] == {
        "param",
        "p",
        "prm",
    }

    ret = cli_doc("tree.test.multiple_tuple_alias", "--output=json")
    assert ret["parameters"]["param"]["aliases"] == ["p", "param", "prm"]
    assert ret["parameters"]["param"]["annotation"] == "<class 'str'>"


def test_tuple_alias_dataclass(hub, cli_doc):
    assert hub.tree.test.tuple_alias_dataclass(param=1) == 1
    assert hub.tree.test.tuple_alias_dataclass(p=1) == 1
    assert hub.tree.test.tuple_alias_dataclass(prm=1) == 1
    assert hub.tree.test.tuple_alias_dataclass(1) == 1

    assert hub.tree.test.tuple_alias_dataclass.param_aliases["param"] == {
        "param",
        "p",
        "prm",
    }

    ret = cli_doc("tree.test.tuple_alias_dataclass", "--output=json")
    assert ret["parameters"]["param"]["aliases"] == ["p", "param", "prm"]
    assert ret["parameters"]["param"]["annotation"] == {
        "typing.List[types.Tag]": {
            "Key": {"annotation": "<class 'str'>", "default": None},
            "Value": {"annotation": "<class 'str'>", "default": None},
        }
    }


def test_computed(hub, cli_doc):
    assert hub.tree.test.computed(param=1) == 1
    assert hub.tree.test.computed(1) == 1

    ret = cli_doc("tree.test.computed", "--output=json")
    assert ret["parameters"]["param"]["annotation"] == "<class 'str'>"
    assert (
        ret["parameters"]["param"]["sub_type"]
        == "<class 'dict_tools.typing.ComputedValue'>"
    )


def test_computed_nest(hub, cli_doc):
    assert hub.tree.test.computed_nest(param=1) == 1
    assert hub.tree.test.computed_nest(1) == 1

    ret = cli_doc("tree.test.computed_nest", "--output=json")
    assert ret["parameters"]["param"]["annotation"] == {
        "typing.List[types.Tag]": {
            "Key": {"annotation": "<class 'str'>", "default": None},
            "Value": {"annotation": "<class 'str'>", "default": None},
        }
    }
    assert (
        ret["parameters"]["param"]["sub_type"]
        == "<class 'dict_tools.typing.ComputedValue'>"
    )


def test_computed_alias(hub, cli_doc):
    assert hub.tree.test.computed_alias(param=1) == 1
    assert hub.tree.test.computed_alias(p=1) == 1
    assert hub.tree.test.computed_alias(1) == 1
    assert hub.tree.test.computed_alias.param_aliases["param"] == {
        "param",
        "p",
    }

    ret = cli_doc("tree.test.computed_alias", "--output=json")
    assert ret["parameters"]["param"]["aliases"] == ["p", "param"]
    assert ret["parameters"]["param"]["annotation"] == "<class 'str'>"
    assert (
        ret["parameters"]["param"]["sub_type"]
        == "<class 'dict_tools.typing.ComputedValue'>"
    )
