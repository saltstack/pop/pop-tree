def test_choice_enum(cli_doc):
    ret = cli_doc("tree.choice.choice_enum")
    assert ret["parameters"]["option"]["choices"] == ["a", "b", "c"]


def test_choice_literal(cli_doc):
    ret = cli_doc("tree.choice.choice_literal")
    assert ret["parameters"]["option"]["choices"] == ["a", "b", "c"]


def test_doc_example(cli_doc):
    ret = cli_doc("tree.choice.my_function")
    assert ret["parameters"]["param1"]["choices"] == ["choice1", "choice2"]
    assert ret["parameters"]["param1"]["annotation"] == "<class 'str'>"
    assert ret["parameters"]["param2"]["choices"] == [1, 2]
    assert ret["parameters"]["param2"]["annotation"] == "<enum 'Choice'>"
