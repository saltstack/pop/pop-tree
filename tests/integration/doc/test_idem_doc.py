def test_list_contract_subs(cli_doc):
    """
    Verify that we have the ability to list contracts with 'idem doc'
    """
    cli_doc("tree._contract_subs")


def test_get_contract_sub_from_list(cli_doc):
    """
    Verify that we have the ability  access a specific contract in the list with dot ref syntax
    """
    cli_doc("tree._contract_subs.0")


def test_get_contract_sub_from_list_bracket(cli_doc):
    """
    Verify that we have the ability  access a specific contract in the list with bracket syntax
    """
    cli_doc("tree._contract_subs[0]")


def test_list_recursive_contract_subs(cli_doc):
    """
    Verify that we have the ability to list contracts with 'idem doc'
    """
    cli_doc("tree._recursive_contract_subs")


def test_get_recursive_contract_sub_from_list(cli_doc):
    """
    Verify that we have the ability  access a specific contract in the list with dot ref syntax
    """
    cli_doc("tree._recursive_contract_subs.0")


def test_get_recursive_contract_sub_from_list_bracket(cli_doc):
    """
    Verify that we have the ability  access a specific contract in the list with bracket syntax
    """
    cli_doc("tree._recursive_contract_subs[0]")


def test_get_specific_contract_func(cli_doc):
    """
    Verify that we have the ability  access a specific contract in the list with bracket syntax
    """
    cli_doc("tree._recursive_contract_subs.0.choice.pre")


def test_doc_example(cli_doc):
    """
    Verify that idem doc lists parameters
    """
    ret = cli_doc("tree.choice.my_function")
    assert ret
    assert ret["parameters"]["param1"]["choices"] == ["choice1", "choice2"]
    assert ret["parameters"]["param1"]["annotation"] == "<class 'str'>"
    assert ret["parameters"]["param2"]["choices"] == [1, 2]
    assert ret["parameters"]["param2"]["annotation"] == "<enum 'Choice'>"
